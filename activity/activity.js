// S28 Activity Template:


// 1.) Insert a single room using the updateOne() method.
// Code here:

// 2.) Insert multiple rooms using the insertMany() method.
// Code here:

// 3.) Use find() method to search for a room with a name "double".
// Code here:

// 4.) Use the updateOne() method to update the queen room and set the available rooms to 0.
// Code here:

// 5.) Use the deleteMany method to delete all rooms that have 0 rooms avaialable.
// Code here:


db.rooms.insertOne({
	"name" : "single",
	"accomodates" : 2,
	"price" : 1000,
	"description" : "A simple room with all the basic necessities",
	"rooms_available" : 10,
	"isAvailable" : false
});

db.rooms.insertMany([
{
	"name" : "double",
	"accomodates" : 3,
	"price" : 2000,
	"description" : "A room fit for a small family going in vacation",
	"rooms_available" : 5,
	"isAvailable" : false
},
{
"name" : "king",
	"accomodates" : 5,
	"price" : 3000,
	"description" : "A room fit for a medium family going in vacation",
	"rooms_available" : 5,
	"isAvailable" : false
}]);

db.rooms.find({
	"name" : "double"
});


db.rooms.updateOne(
	{
		"name" : "king"
	},
	{
		$set:{
			"rooms_available" : 0
		}
	});

db.rooms.deleteMany({
"rooms_available" : 0
})