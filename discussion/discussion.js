//  CRUD Operations :
/*
	1. Create - insert
	2. Read - Find
	3. Update - update
	4. Destroy - Delete

*/

// Insert Method () - Create documents in our DB.
/*

	Syntax:
		Isert One Document:
			db.collectionName.insertOner({
				"fieldA" : "valueA",
				"fieldB" : "valueB"

			});

		Insert Many Documents:
		 db.collectionName.insertMany{[
			{
				"fieldA" : "valueA",
				"fieldB" : "valueB"
	
			},
			{
				"fieldA" : "valueA",
				"fieldB" : "valueB"

			},
			{
				"fieldA" : "valueA",
				"fieldB" : "valueB"

			}

		 ]}


*/

//  upon execution on Robo 3t make sure not ot execute your data seeral times as it will duplicate

db.users.insertOne({
	"firstName" : "Jane",
	"lastName" : "Dela Cruz",
	"age" : 21,
	"email" : "janedc@gmail.com",
	"company" : "none"

});

db.users.insertMany([
		{
			"firstName" : "Stephen",
			"lastName" : "Hawkins",
			"age" : 67,
			"email" : "Stephenhawkins@gmail.com",
			"department" : "none"
		},
		{
			"firstName" : "Niel",
			"lastName" : "Armstrong",
			"age" : 82,
			"email" : "nielarmstrong@gmail.com",
			"department" : "none"
		}
	]);


//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

db.coures.insertMany([
	{
		"name" : "Javascript 101",
		"price" : 5000,
		"description" : "Introduction to Javascript",
		"isActive" : true
	},
	{
		"name" : "HTML 101",
		"price" : 2000,
		"description" : "Introduction to HTML",
		"isActive" : true
	},
	{
		"name" : "CSS 101",
		"price" : 2500,
		"description" : "Introduction to CSS",
		"isActive" : true
	}
]);


// Find document/ Method() -Read

/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		
		db.collectionName.find({"criteria" : "value"}) - this will retrieve all the documents that will match our criteria. 


		db.collectionName.find({"criteria" : "value"}) - this will return the first document in our collection that match our criteria.

		db.collectionName.find({}) - this will return the  first document in our collection.

*/

db.users.find();

db.users.find({
	"firstName" : "Jane"
});

db.users.find({
	"firstName" : "Niel",
	"age" : 82
});


// Update Documents/Methods - Updates our documents in our collection

/*

	Syntax:
	updateOne() 
		db.collectionName.updateOne(
		{
			"criteria" : "value"
		},
		{
			$set:{
				"feildToUpdated" : "updatedValue"
			}
		}
		);

	
	updateMany()

	Syntax:
		db.collectionName.updateMany(
		{
			"criteria" : "value"
		},
		{
			$set:{
				"feildToUpdated" : "updatedValue"
			}
		}
		);

*/

db.users.insertOne({
	"firstName" : "Test",
	"lastName" : "Test",
	"age" : 0,
	"email" : "test@gmail.com",
	"department" : "none"
});

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawkings",
			"age": 67,
			"email": "Stephenhawkings@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Niel",
			"lastName": "Armstrong",
			"age": 82,
			"email": "nielarmstrong@gmail.com",
			"department": "none"
		}
	]);
// Updating one document

db.users.updateOne(
	{
		"firstName" : "Test"
	},
	{
		$set: {
			"firstName" : "Bill",
			"lastName" : "Gates",
			"age" : 65,
			"email" : "billgates@gmail.com",
			"department" : "operations",
			"status" : "active"
		}
	}

);


// Remove a field
db.users.updateOne(
		{
			"firstName" : "Bill"
		},
		{
			$unset:{
				"status":"active"
			}
		}
	);

// Updating multiple documents
db.users.updateMany(
	{
		"department" : "none"
	},
	{
		$set:{
			"department" : "HR"
		}
	}
	);

db.users.updateOne({},
	{
		$set:{
			"dapartment" : "operations"
		}
	}
);


db.coures.updateOne(
{
	"name": "HTML 101"
},
{
	$set:{
		"isActive" : false
	}
}
);

db.coures.updateMany(
{},
{
	$set:{
		"enrollees" : 10
	}
})


// Destroy/Delete Documents/Method - deleting documents from our collection

db.users.insertOne({
	"firstName" : "Test"
});

// Deleting a single document

/*
	Syntax:
		db.collectionName.deleteOne({"criteria" : "value"})
*/
db.users.deleteOne(
	{
		"firstName": "Test"
	});

// Deleting multiple documents
/*
	Syntax:
		db.collectionName.deleteMany({"criteria": "value"})

*/

db.users.deleteMany({
	"department" : "HR"
});

db.coures.deleteMany({});